package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

func archiveIncidentChannels() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()
	slackClient := slack.New(*archiveIncidentChannelsAccessToken, slack.OptionHTTPClient(httpClient))
	gitlabClient := newGitLabClient(*archiveIncidentChannelsGitlabAPIToken, *archiveIncidentChannelsGitlabAPIBaseURL, httpClient)

	monitor := woodhouse.NewJobMonitor(prometheus.NewRegistry(), *pushGatewayURL)
	run := func(ctx context.Context) error {
		return slackutil.ArchiveIncidentChannels(
			ctx, logger, slackClient, gitlabClient,
			*archiveIncidentChannelsGitlabProductionProjectPath,
			*archiveIncidentChannelsPrefix,
			*archiveIncidentChannelsDryRun,
		)
	}
	return monitor.Run(ctx, "archive_incident_channels", time.Hour*25, run)
}

func updateOncallUsergroups() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	httpClient := newHTTPClient()

	pdClient := gopagerduty.NewClient(*slackUpdateOncallUsergroupsPagerdutyAPIToken)
	pdClient.HTTPClient = httpClient
	pager := &pagerduty.Pager{HTTPClient: httpClient, PagerdutyClient: pdClient}

	slackClient := slack.New(*slackUpdateOncallUsergroupsAccessToken, slack.OptionHTTPClient(httpClient))

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		<-signals
		logger.Log("event", "signal_received")
		cancel()
	}()

	first := make(chan interface{}, 1)
	first <- nil

	t := time.NewTicker(*slackUpdateOncallUsergroupsInterval)
	for {
		select {
		case <-first:
		case <-t.C:
		case <-ctx.Done():
			return ctx.Err()
		}

		err := slackutil.UpdateOncallUsergroups(
			ctx, logger, slackClient,
			*pager,
			*slackUpdateOncallUsergroupsGroupID,
			*slackUpdateOncallUsergroupsPagerdutyEscalationPolicy,
			*slackUpdateOncallUsergroupsDryRun,
		)

		if err != nil {
			logger.Log("msg", "failed to update on-call user groups in slack", "err", err)
		}
	}
}
