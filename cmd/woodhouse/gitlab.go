package main

import (
	"context"
	"fmt"
	"os"
	"sort"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
)

func notifyMirroredMr() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*2)
	defer cancel()
	client := newGitLabClient(*gitlabNotifyMirroredMrToken, *gitlabNotifyMirroredMrBaseURL, newHTTPClient())
	return gitlabutil.NotifyMergeRequestFromMirrorSource(ctx, client, logger, *gitlabNotifyMirroredMrProjectPath, *gitlabNotifyMirroredMrPipelineStatus, *gitlabNotifyMirroredMrDryRun)
}

func followRemotePipeline() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Hour)
	defer cancel()
	client := newGitLabClient(*gitlabFollowRemotePipelineToken, *gitlabFollowRemotePipelineBaseURL, newHTTPClient())

	// Assume that this will only be used on mirrors with identical project paths.
	// If this assumption doesn't hold, we can always make it configurable.
	return gitlabutil.FollowRemotePipeline(ctx, logger, client, os.Getenv("CI_PROJECT_PATH"), os.Getenv("CI_COMMIT_SHA"))
}

func listPrivateEmails() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()

	pdClient := gopagerduty.NewClient(*gitlabListPrivateEmailsPagerdutyAPIToken)
	pdClient.HTTPClient = httpClient
	gitlabClient := newGitLabClient(*gitlabListPrivateEmailsGitlabAPIToken, *gitlabListPrivateEmailsGitlabAPIBaseURL, httpClient)

	privateEmails, err := gitlabutil.ListPrivateEmails(ctx, pdClient, gitlabClient, *gitlabListPrivateEmailsPagerdutyEscalationPolicy)
	if err != nil {
		return err
	}

	sort.Strings(privateEmails)

	for _, email := range privateEmails {
		fmt.Println(email)
	}

	return nil
}
