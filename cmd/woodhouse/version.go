package main

import (
	"fmt"

	"github.com/prometheus/common/version"
)

func printVersion() error {
	fmt.Println(version.Print("woodhouse"))
	return nil
}
