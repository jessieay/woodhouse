package main

import (
	"context"
	"time"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/alertmanager"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
)

func notifyExpiringSilences() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	httpClient := newHTTPClient()
	slackClient := slack.New(*alertmanagerSlackAccessToken, slack.OptionHTTPClient(httpClient))

	alertmanagerClient := alertmanager.NewClient(httpClient, *alertmanagerBaseURL)

	silences, err := alertmanager.GetExpiringSilences(ctx, alertmanager.GetExpiringSilencesOptions{
		AlertmanagerClient: alertmanagerClient,
		Cutoff:             *alertmanagerNotifyExpiringSilencesCutoff,
		StartTime:          time.Now().UTC(),
	})

	if err != nil {
		return err
	}

	return slackutil.NotifyExpiringSilences(ctx, logger, slackutil.NotifyExpiringSilencesOptions{
		Silences:                    silences,
		SlackClient:                 slackClient,
		AlertmanagerExternalBaseURL: *alertmanagerExternalBaseURL,
		SlackChannelID:              *alertmanagerSlackChannelID,
		Cutoff:                      *alertmanagerNotifyExpiringSilencesCutoff,
		DryRun:                      *alertmanagerNotifyExpiringSilencesDryRun,
	})
}
