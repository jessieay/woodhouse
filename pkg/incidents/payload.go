package incidents

import (
	"errors"

	"github.com/slack-go/slack"
)

const (
	EOCSelection  = "pageEOC"
	IMSelection   = "pageIM"
	CMOCSelection = "pageCMOC"
)

type IncidentPayload struct {
	incidentPayload *slack.InteractionCallback
}

func NewIncidentPayload(payload *slack.InteractionCallback) (*IncidentPayload, error) {
	if payload == nil {
		return nil, errors.New("NewIncidentPayload: payload is nil!")
	}
	return &IncidentPayload{incidentPayload: payload}, nil
}

func (p *IncidentPayload) Title() string {
	return p.incidentPayload.View.State.Values["title"]["title"].Value
}

func (p *IncidentPayload) SeverityLabel() string {
	return p.incidentPayload.View.State.Values["severity"]["severity"].SelectedOption.Value
}

func (p *IncidentPayload) PrivateMetadata() string {
	return p.incidentPayload.View.PrivateMetadata
}

func (p *IncidentPayload) IsInternallyFacing() bool {
	for _, labelObject := range p.incidentPayload.View.State.Values["extralabels"]["extralabels"].SelectedOptions {
		if labelObject.Value == "backstage" {
			return true
		}
	}
	return false
}

func (p *IncidentPayload) ServiceLabel() string {
	return p.incidentPayload.View.State.Values["service"]["service"].SelectedOption.Value
}

func (p *IncidentPayload) SelectedTasks() []string {
	tasks := []string{}
	for _, selectedTask := range p.incidentPayload.View.State.Values["tasks"]["tasks"].SelectedOptions {
		tasks = append(tasks, selectedTask.Value)
	}

	return tasks
}

func (p *IncidentPayload) IMSelected() bool {
	for _, s := range p.SelectedTasks() {
		if s == IMSelection {
			return true
		}
	}
	return false
}

func (p *IncidentPayload) IsConfidential() bool {
	for _, field := range p.incidentPayload.View.State.Values["confidential"]["confidential"].SelectedOptions {
		switch field.Value {
		case "confidential":
			return true
		}
	}

	return false
}

func (p *IncidentPayload) ExtraLabels() []string {
	extraLabels := []string{}
	for _, labelObject := range p.incidentPayload.View.State.Values["extralabels"]["extralabels"].SelectedOptions {
		extraLabels = append(extraLabels, labelObject.Value)
	}

	return extraLabels
}
