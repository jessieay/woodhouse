package incidents

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"regexp"
	"sort"
	"strings"
	"text/template"
	"time"

	gostatusio "github.com/statusio/statusio-go"

	"github.com/iancoleman/strcase"
	"github.com/sethvargo/go-retry"
	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/statusio"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

const (
	DeclareModalCallbackID   = "incident-declare"
	PostStatuspageCallbackID = "incident-post-statuspage"

	// Channel topics and purposes both have a documented max length of 250
	// characters. Accomodating a realistic URL for the markdown link and the
	// wrapper text, we could get away with a slightly higher limit, but let's
	// play it safe.
	issueTitleMaxLengthForSlack = 85
	spinTheWheelNumDays         = 7
	spinTheWheelSeverity        = "severity::2"

	incidentStatusTitle           = "Incident Status"
	defaultIncidentMessageSubject = "Status Notification from GitLab System Status"
	defaultIncidentTitle          = "Undetermined problem - details to follow"
	defaultIncidentDetails        = "We have detected a problem related to GitLab.com services and are actively investigating.\n\nMore details to follow."

	subcommandDeclare        = "declare"
	subcommandSpinTheWheel   = "spin-the-wheel"
	subcommandPostStatuspage = "post-statuspage"
)

var (
	whitespace  = regexp.MustCompile(`\s+`)
	subCommands = []string{
		subcommandDeclare,
		subcommandSpinTheWheel,
		subcommandPostStatuspage,
	}
)

type SlashCommand struct {
	SlackClient                 *slack.Client
	JobRunner                   *woodhouse.AsyncJobRunner
	IncidentChannelID           string
	GitlabProductionProjectPath string
	GitlabClient                *gitlab.Client
	StatusioClient              *statusio.Client
}

type IncidentContext struct {
	UserID            string
	Username          string
	AnnounceChannelID string
	TeamID            string
}

func (s *SlashCommand) Handle(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	subcommandArgs := whitespace.Split(command.Text, -1)
	if len(subcommandArgs) != 1 {
		s.help(event, commandName, command, w, req)
		return
	}

	switch subcommandArgs[0] {
	case subcommandDeclare:
		s.declare(event, command, w, req)
	case subcommandSpinTheWheel:
		s.spinTheWheel(event, command, w, req)
	case subcommandPostStatuspage:
		s.postStatuspage(event, command, w, req)
	default:
		s.help(event, commandName, command, w, req)
	}
}

func (s *SlashCommand) postStatuspage(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	if s.StatusioClient == nil {
		msg := slack.Msg{
			Text:         ":status_warning: This feature is *DISABLED* as you're missing at least one of the status.io configuration options (check `--help`)",
			ResponseType: slack.ResponseTypeEphemeral,
		}
		if err := json.NewEncoder(w).Encode(msg); err != nil {
			event.With("error", err, "message", "error responding to slash command")
			return
		}

		return
	}

	msg := slack.Msg{
		Text:         fmt.Sprintf(":roger: Please fill in the form to create a statuspage incident at <%s>", s.StatusioClient.BaseURL()),
		ResponseType: slack.ResponseTypeEphemeral,
	}
	if err := json.NewEncoder(w).Encode(msg); err != nil {
		event.With("error", err, "message", "error responding to slash command")
		return
	}

	s.JobRunner.RunAsyncJob("present_post_statuspage_modal", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return s.presentPostStatuspageModal(ctx, event, &command)
	})
}

func (s *SlashCommand) spinTheWheel(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	issueInterval := time.Now().Add(-24 * spinTheWheelNumDays * time.Hour)
	issues, _, err := s.GitlabClient.Issues.ListProjectIssues(
		s.GitlabProductionProjectPath, &gitlab.ListProjectIssuesOptions{
			Labels:       []string{"Incident::Resolved", spinTheWheelSeverity},
			CreatedAfter: &issueInterval,
		},
	)
	if err != nil {
		event.With("error", err, "message", "error fetching issues")
		return
	}

	if len(issues) == 0 {
		if err := json.NewEncoder(w).Encode(slack.Msg{
			Text:         ":wheel_of_dharma: No issues available for spin-the-wheel :yay:",
			ResponseType: slack.ResponseTypeInChannel,
		}); err != nil {
			event.With("error", err, "message", "error responding to slash command")
			return
		}
		return
	}

	rand.Seed(time.Now().Unix())
	randIssue := issues[rand.Intn(len(issues))]

	if err := json.NewEncoder(w).Encode(slack.Msg{
		Text:         fmt.Sprintf(":wheel_of_dharma: Picking random sev2 incident %s for spin-the-wheel!", randIssue.WebURL),
		ResponseType: slack.ResponseTypeInChannel,
	}); err != nil {
		event.With("error", err, "message", "error responding to slash command")
		return
	}
}

func (s *SlashCommand) declare(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	// Do not alarm the on-call by echoing back the slash command, or posting any
	// messages visible by anyone other than the caller. These would be
	// unactionable until the modal form has been submitted.
	msg := slack.Msg{
		Text:         fmt.Sprintf("I'm on it! Please fill in the form, which will declare an incident in <#%s>.", s.IncidentChannelID),
		ResponseType: slack.ResponseTypeEphemeral,
	}
	if err := json.NewEncoder(w).Encode(msg); err != nil {
		event.With("error", err, "message", "error responding to slash command")
		return
	}

	s.JobRunner.RunAsyncJob("present_incident_declare_modal", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return s.presentDeclareModal(ctx, event, &command)
	})
}

func (s *SlashCommand) help(
	event *instrumentation.Event, commandName string, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	if err := json.NewEncoder(w).Encode(slack.Msg{
		ResponseType: slack.ResponseTypeEphemeral,
		Text:         fmt.Sprintf("Usage: %s <%s>", commandName, strings.Join(subCommands, " | ")),
	}); err != nil {
		event.With("error", err, "message", "error writing HTTP response")
	}
}

func (s *SlashCommand) presentPostStatuspageModal(ctx context.Context, event *instrumentation.Event, command *slack.SlashCommand) error {
	callbackData, err := json.Marshal(IncidentContext{
		UserID:            command.UserID,
		Username:          command.UserName,
		AnnounceChannelID: s.IncidentChannelID,
		TeamID:            command.TeamID,
	})
	if err != nil {
		return err
	}

	modal := slack.ModalViewRequest{
		Type:            slack.VTModal,
		Title:           slackapps.PlainText("Post Statuspage"),
		Close:           slackapps.PlainText("Cancel"),
		Submit:          slackapps.PlainText("Create incident"),
		CallbackID:      PostStatuspageCallbackID,
		PrivateMetadata: string(callbackData),
	}

	preTitleHelpText := slack.NewTextBlockObject(slack.MarkdownType, "*This form allows you to quickly create an incident on status.io to tell the world that we're aware that something isn't right and we're looking into it.*", false, false)
	preTitleHelpBlock := slack.NewSectionBlock(preTitleHelpText, nil, nil)

	updateIncidentHelpText := slack.NewTextBlockObject(slack.MarkdownType, "_To *update* this incident, you need to follow <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#stage-2-incident-updates|these instructions>._", false, false)
	updateIncidentHelpBlock := slack.NewSectionBlock(updateIncidentHelpText, nil, nil)

	titleBlock := slackapps.TextInputBlockWithInitialValue("Title", defaultIncidentTitle)
	subjectBlock := slackapps.TextInputBlockWithInitialValue("Message Subject", defaultIncidentMessageSubject)
	detailsBlockElement := slack.PlainTextInputBlockElement{
		Type:         slack.METPlainTextInput,
		ActionID:     "details",
		InitialValue: defaultIncidentDetails,
		Multiline:    true,
	}
	detailsBlock := slack.NewInputBlock("details", slackapps.PlainText("Details"), detailsBlockElement)

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, preTitleHelpBlock, updateIncidentHelpBlock, titleBlock, subjectBlock, detailsBlock)

	incidentStatusId := strcase.ToLowerCamel(incidentStatusTitle)
	var incidentStatusSelectOpts []*slack.OptionBlockObject
	for _, status := range statusio.IncidentStatuses {
		incidentStatusSelectOpts = append(incidentStatusSelectOpts, slack.NewOptionBlockObject(status.ToString(), slackapps.PlainText(status.Name), nil))
	}
	incidentStatusInput := slack.NewOptionsSelectBlockElement("static_select", slackapps.PlainText("Choose incident status"), incidentStatusId, incidentStatusSelectOpts...)
	incidentStatusInput.InitialOption = incidentStatusSelectOpts[statusio.IncidentStatuses.GetDefaultStatusIndex()]
	incidentStatusBlock := slack.NewInputBlock(incidentStatusId, slackapps.PlainText(incidentStatusTitle), incidentStatusInput)

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, incidentStatusBlock)

	nonCicdComponents, err := s.StatusioClient.GetInfrastructureListFilterByFunction("non CI/CD components", func(name string) bool {
		return !strings.HasPrefix(name, "CI/CD")
	})

	if err != nil {
		return fmt.Errorf("unable to fetch components from status.io: %w", err)
	}

	var nonCicdComponentOptions []*slack.OptionBlockObject
	for _, component := range nonCicdComponents {
		componentOption := slack.NewOptionBlockObject(component.ID, slackapps.PlainText(component.Name), nil)
		nonCicdComponentOptions = append(nonCicdComponentOptions, componentOption)
	}

	nonCicdCheckboxComponents := slack.NewCheckboxGroupsBlockElement("components", nonCicdComponentOptions...)
	nonCicdCheckboxComponents.InitialOptions = append(nonCicdCheckboxComponents.InitialOptions, nonCicdComponentOptions...)

	nonCicdComponentsBlock := slack.NewInputBlock(
		"nonCicdComponents", slackapps.PlainText("Components"),
		nonCicdCheckboxComponents,
	)
	nonCicdComponentsBlock.Optional = true

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, nonCicdComponentsBlock)

	cicdComponents, err := s.StatusioClient.GetInfrastructureListFilterByFunction("CI/CD components", func(name string) bool {
		return strings.HasPrefix(name, "CI/CD")
	})
	if err != nil {
		return fmt.Errorf("unable to fetch components from status.io: %w", err)
	}

	var cicdComponentOptions []*slack.OptionBlockObject
	for _, component := range cicdComponents {
		componentOption := slack.NewOptionBlockObject(component.ID, slackapps.PlainText(component.Name), nil)
		cicdComponentOptions = append(cicdComponentOptions, componentOption)
	}

	cicdCheckboxComponents := slack.NewCheckboxGroupsBlockElement("components", cicdComponentOptions...)
	cicdCheckboxComponents.InitialOptions = append(cicdCheckboxComponents.InitialOptions, cicdComponentOptions...)

	cicdComponentsBlock := slack.NewInputBlock(
		"cicdComponents", slackapps.PlainText("CI/CD Components"),
		cicdCheckboxComponents,
	)
	cicdComponentsBlock.Optional = true

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, cicdComponentsBlock)

	_, err = s.SlackClient.OpenView(command.TriggerID, modal)
	if err != nil {
		return err
	}

	return nil
}

func (s *SlashCommand) presentDeclareModal(ctx context.Context, event *instrumentation.Event, command *slack.SlashCommand) error {
	callbackData, err := json.Marshal(IncidentContext{
		UserID:            command.UserID,
		Username:          command.UserName,
		AnnounceChannelID: s.IncidentChannelID,
		TeamID:            command.TeamID,
	})
	if err != nil {
		return err
	}

	// Gather all errors and return them at the end, to do as much useful work as
	// possible for the modal dialog
	var errs []error

	modal := slack.ModalViewRequest{
		Type:            slack.VTModal,
		Title:           slackapps.PlainText("Declare an incident"),
		Close:           slackapps.PlainText("Close"),
		Submit:          slackapps.PlainText("Submit"),
		CallbackID:      DeclareModalCallbackID,
		PrivateMetadata: string(callbackData),
	}

	titleInitialValue := time.Now().UTC().Format("2006-01-02") + ": "
	titleBlock := slackapps.TextInputBlockWithInitialValue("Title", titleInitialValue)
	severityHelpLink := slack.NewTextBlockObject(slack.MarkdownType, "<https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident|Handbook: how to fill out this form>", false, false)
	severityHelpBlock := slack.NewSectionBlock(severityHelpLink, nil, nil)
	severityBlock := slackapps.SelectInputBlock(
		"Severity", "Choose a severity",
		2,
		Severity(1).IssueLabel(),
		Severity(2).IssueLabel(),
		Severity(3).IssueLabel(),
		Severity(4).IssueLabel(),
	)
	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, titleBlock, severityHelpBlock, severityBlock)

	pageEOC := slack.NewOptionBlockObject("pageEOC", slackapps.PlainText("Page engineer on-call"), nil)
	pageIM := slack.NewOptionBlockObject("pageIM", slackapps.PlainText("Page incident manager"), nil)
	pageCMOC := slack.NewOptionBlockObject("pageCMOC", slackapps.PlainText("Page communications manager on-call"), nil)

	checkboxGroupsBlock := slack.NewCheckboxGroupsBlockElement(
		"tasks",
		pageEOC,
		pageIM,
		pageCMOC,
	)
	checkboxGroupsBlock.InitialOptions = append(checkboxGroupsBlock.InitialOptions, pageEOC, pageIM, pageCMOC)

	tasksBlock := slack.NewInputBlock(
		"tasks", slackapps.PlainText("If severity == 1 or 2"),
		checkboxGroupsBlock,
	)
	tasksBlock.Optional = true

	extraLabelsBlock := slack.NewInputBlock(
		"extralabels", slackapps.PlainText("Incident Issue Labels"),
		slack.NewCheckboxGroupsBlockElement(
			"extralabels",
			slack.NewOptionBlockObject("backstage", slackapps.PlainText("backstage, incidents with internal impact only"), nil),
			slack.NewOptionBlockObject("blocks deployments", slackapps.PlainText("blocks deployments, to automatically halt deployments"), nil),
		),
	)
	extraLabelsBlock.Optional = true

	optionalLabelsHandbookLink := slack.NewTextBlockObject(slack.MarkdownType, "<https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#optional-labeling|Handbook: optional labels>", false, false)
	optionalLabelLinkBlock := slack.NewSectionBlock(optionalLabelsHandbookLink, nil, nil)

	confidentialBlock := slack.NewInputBlock(
		"confidential", slackapps.PlainText("Confidential"),
		slack.NewCheckboxGroupsBlockElement(
			"confidential",
			slack.NewOptionBlockObject("confidential", slackapps.PlainText("Mark issue as confidential"), nil),
		),
	)
	confidentialBlock.Optional = true

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, tasksBlock, confidentialBlock, extraLabelsBlock, optionalLabelLinkBlock)

	_, err = s.SlackClient.OpenView(command.TriggerID, modal)
	if err != nil {
		errs = append(errs, err)
	}

	return woodhouse.MultiError(errs)
}

type PostStatuspageModalHandler struct {
	JobRunner      *woodhouse.AsyncJobRunner
	SlackClient    *slack.Client
	StatusioClient *statusio.Client
}

func (i *PostStatuspageModalHandler) Validate(
	event *instrumentation.Event, payload slack.InteractionCallback,
	w http.ResponseWriter, req *http.Request,
) *slack.ViewSubmissionResponse {
	return nil
}

func (i *PostStatuspageModalHandler) Handle(
	event *instrumentation.Event, payload slack.InteractionCallback,
	w http.ResponseWriter, req *http.Request,
) {
	i.JobRunner.RunAsyncJob("post_statuspage", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return i.postStatuspage(ctx, event, &payload)
	})
}

func (i *PostStatuspageModalHandler) postStatuspage(ctx context.Context, event *instrumentation.Event, payload *slack.InteractionCallback) error {
	event.With("job", "post_statuspage")

	var incidentContext IncidentContext
	if err := json.Unmarshal([]byte(payload.View.PrivateMetadata), &incidentContext); err != nil {
		return err
	}

	selectedOptions := append(payload.View.State.Values["cicdComponents"]["components"].SelectedOptions, payload.View.State.Values["nonCicdComponents"]["components"].SelectedOptions...)
	if len(selectedOptions) == 0 {
		if _, msgErr := i.SlackClient.PostEphemeral(
			incidentContext.AnnounceChannelID,
			incidentContext.UserID,
			slack.MsgOptionText(":status_failed: unable to process your statuspage request: *you must select at least one component*", false),
		); msgErr != nil {
			event.With("send_ephemeral_msg", msgErr)
		}
		return errors.New("at least one component must be selected")
	}

	var selectedComponentIDs []string
	for _, selectedComponent := range selectedOptions {
		selectedComponentIDs = append(selectedComponentIDs, selectedComponent.Value)
	}

	incident := gostatusio.Incident{
		IncidentName:    payload.View.State.Values["title"]["title"].Value,
		IncidentDetails: payload.View.State.Values["details"]["details"].Value,
		MessageSubject:  payload.View.State.Values["messageSubject"]["messageSubject"].Value,
	}

	incidentCreateResponse, err := i.StatusioClient.CreateIncident(
		incident,
		selectedComponentIDs,
		payload.View.State.Values["incidentStatus"]["incidentStatus"].SelectedOption.Value,
	)
	if err != nil {
		if _, msgErr := i.SlackClient.PostEphemeral(
			incidentContext.AnnounceChannelID,
			incidentContext.UserID,
			slack.MsgOptionText(":status_failed: unable to process your statuspage request: error creating incident", false),
		); msgErr != nil {
			event.With("send_ephemeral_msg", msgErr)
		}
		event.With("create_incident", err)
		return err
	}

	if _, _, msgErr := i.SlackClient.PostMessage(
		incidentContext.AnnounceChannelID,
		slack.MsgOptionText(fmt.Sprintf(":status_success: A statuspage incident has been created: <%s/%s> (<%s|manage>)",
			i.StatusioClient.GetIncidentsBaseURL(),
			incidentCreateResponse.Result,
			i.StatusioClient.GetIncidentDashboardURL(incidentCreateResponse.Result),
		), false),
		slack.MsgOptionDisableLinkUnfurl(),
		slack.MsgOptionDisableMediaUnfurl(),
	); msgErr != nil {
		event.With("send_ephemeral_msg", msgErr)
	}

	return nil
}

type DeclareModalHandler struct {
	JobRunner                   *woodhouse.AsyncJobRunner
	SlackClient                 *slack.Client
	GitlabClient                *gitlab.Client
	GitlabGraphQLClient         *gitlabutil.GraphQLClient
	GitlabProductionProjectPath string
	GitLabProfileSlackFieldID   string
	GitLabIssueType             string

	IncidentChannelNamePrefix string
	IncidentCallURL           string

	Pager                         *pagerduty.Pager
	PagerdutyIntegrationKeyEOC    string
	PagerdutyIntegrationKeyIMOC   string
	PagerdutyIntegrationKeyCMOC   string
	PagerdutyEscalationPolicyEOC  string
	PagerdutyEscalationPolicyIMOC string
}

func (i *DeclareModalHandler) Validate(
	event *instrumentation.Event, payload slack.InteractionCallback,
	w http.ResponseWriter, req *http.Request,
) *slack.ViewSubmissionResponse {
	validationErrors := map[string]string{}

	if validTitle, _ := regexp.MatchString("^\\d+-\\d+-\\d+: .+$", payload.View.State.Values["title"]["title"].Value); !validTitle {
		validationErrors["title"] = "You must specify a valid title (format: \"YYYY-MM-DD: <title>\")"
	}

	if len(validationErrors) != 0 {
		return slack.NewErrorsViewSubmissionResponse(validationErrors)
	}

	return nil
}

func (i *DeclareModalHandler) Handle(
	event *instrumentation.Event, payload slack.InteractionCallback,
	w http.ResponseWriter, req *http.Request,
) {
	i.JobRunner.RunAsyncJob("create_incident", func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		payload, err := NewIncidentPayload(&payload)
		if err != nil {
			return err
		}

		return i.createIncident(ctx, event, payload)
	})
}

// We want to do as much useful work as possible if the GitLab incident issue
// creation fails. This isn't very unrealistic: after all, we're only calling
// this in response to a GitLab incident! If the GitLab incident issue creation
// fails, we still create a Slack channel, and send pages (if requested).
func (i *DeclareModalHandler) createIncident(ctx context.Context, event *instrumentation.Event, payload *IncidentPayload) error {
	event.With("job", "create_incident")

	var incident IncidentContext
	if err := json.Unmarshal([]byte(payload.PrivateMetadata()), &incident); err != nil {
		return err
	}
	title := payload.Title()
	severityLabel := payload.SeverityLabel()
	severity, err := ParseSeverityFromIssueLabel(severityLabel)
	if err != nil {
		return err
	}
	severityEmoji := severity.SlackEmoji()

	introBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, "An *incident* has been reported.", false, false), nil, nil)
	titleBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":gitlab: %s %s :loading:", severityEmoji, title), false, false), nil, nil)
	channelBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, ":slack: :loading:", false, false), nil, nil)
	callBlock := slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":zoom: <%s>", i.IncidentCallURL), false, false), nil, nil)

	var slackDisplayName, slackProfileImage, gitlabUsername string

	profile, err := i.SlackClient.GetUserProfileContext(ctx, &slack.GetUserProfileParameters{UserID: incident.UserID, IncludeLabels: false})
	if err != nil {
		event.With("get_profile_err", err)
		// Empty string will cause Woodhouse's profile picture not to be overriden.
		// We don't return an error here to allow graceful degradation if the
		// profile can't be fetched.
		// Since we can't fetch the Slack profile, we can't make any assumptions
		// about the GitLab username, so just return the Slack username without any
		// `@` to avoid random GitLab users being notified.
		slackDisplayName = incident.Username
		slackProfileImage = ""
		gitlabUsername = incident.Username
	} else {
		slackDisplayName, slackProfileImage, gitlabUsername = slackutil.GetProfileData(
			ctx, event, profile, i.GitLabProfileSlackFieldID)
	}
	_, msgTimestamp, _, err := i.SlackClient.SendMessageContext(
		ctx, incident.AnnounceChannelID,
		slack.MsgOptionBlocks(introBlock, slack.NewDividerBlock(), titleBlock, channelBlock, callBlock),
		slack.MsgOptionUser(incident.UserID),
		slack.MsgOptionUsername(slackDisplayName),
		slack.MsgOptionIconURL(slackProfileImage),

		// This text is used in notifications. If we didn't include it, the
		// notification would say "content cannot be displayed".
		slack.MsgOptionText(fmt.Sprintf("An incident has been reported: %s %s", severityEmoji, title), false),
	)
	if err != nil {
		return err
	}

	// Don't use SendMessageContext to update Slack users. Calls to external
	// systems could themselves time out of their retry loops, and we want to try
	// to update the Slack users anyway.
	updateMsg := func() error {
		_, _, _, err := i.SlackClient.SendMessage(
			incident.AnnounceChannelID,
			slack.MsgOptionUpdate(msgTimestamp),
			slack.MsgOptionBlocks(introBlock, slack.NewDividerBlock(), titleBlock, channelBlock, callBlock),
		)
		return err
	}

	sendMsg := func(msg string) error {
		_, _, _, err := i.SlackClient.SendMessage(
			incident.AnnounceChannelID,
			slack.MsgOptionText(msg, false),
		)
		return err
	}

	// Gather all errors and return them at the end, to do as much useful work as
	// possible even if gitlab.com is totally down, preventing us from creating
	// the incident issue.
	var errs []error

	// We only page for severity::1 and severity::2 incidents
	if severity == 1 || severity == 2 {
		// Tasks correspond 1:1 with a checkbox in the modal
		for i, err := range i.performOptionalTasks(ctx, payload, incident, title, sendMsg) {
			errs = append(errs, err)
			event.With(fmt.Sprintf("optional_task_err_%d", i), err)
			_ = sendMsg(":x: " + err.Error())
		}
	}

	incidentChannel, err := i.createChannel(ctx, title)
	if err != nil {
		channelBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":slack: :x: %s", err), false, false), nil, nil)
		errs = append(errs, err)
	} else {
		channelBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":slack: <#%s>", incidentChannel.ID), false, false), nil, nil)
	}
	_ = updateMsg()

	// Even if there were errors this can still be passed in createIssue to create an issue without assignees.
	onCallGitLabUsers, err := i.getGitLabUsersForOnCalls(ctx, severity, payload)
	if err != nil {
		errs = append(errs, err)
	}

	activeIncidents, err := i.Pager.OpenIncidentsForEscalationPolicy(i.PagerdutyEscalationPolicyEOC)
	if err != nil {
		errs = append(errs, err)
	}

	incidentLabels := []string{}

	for _, i := range activeIncidents {
		if i.AlertName == "" {
			continue
		}

		incidentLabels = append(incidentLabels, fmt.Sprintf("a:%s", i.AlertName))
	}

	issue, err := i.createIssue(ctx, event, payload, gitlabUsername, onCallGitLabUsers, incidentLabels)
	if err != nil {
		_ = sendMsg(":gitlab: :x: Error creating incident issue: " + err.Error())
		_ = sendMsg("Incident issue creation has failed. You might want to follow the workflow for when gitlab.com is totally down: <https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/incidents/README.md#slack-incident-declare-failed-to-create-an-incident-issue>.")
		titleBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":gitlab: %s %s", severityEmoji, title), false, false), nil, nil)
		_ = updateMsg()
		return woodhouse.MultiError(append(errs, err))
	}

	if err := i.GitlabGraphQLClient.SetIssueSeverity(
		issue.IID,
		i.GitlabProductionProjectPath,
		severity.IncidentSeverity(),
	); err != nil {
		errs = append(errs, err)
	}

	titleBlock = slack.NewSectionBlock(slack.NewTextBlockObject(slack.MarkdownType, fmt.Sprintf(":gitlab: <%s|%s %s>", issue.WebURL, severityEmoji, title), false, false), nil, nil)
	_ = updateMsg()

	if incidentChannel != nil {
		newName := i.incidentChannelName(int64(issue.IID))
		if incidentChannel, err = i.SlackClient.RenameConversationContext(ctx, incidentChannel.ID, newName); err != nil {
			errs = append(errs, err)
		}
		if err := i.linkChannelToIssue(ctx, incidentChannel, issue); err != nil {
			errs = append(errs, err)
		}

		if err := i.GitlabGraphQLClient.CreateLinkedResource(
			issue.ID,
			slackapps.ChannelLink(incidentChannel.ID, incident.TeamID),
			"Slack channel here",
		); err != nil {
			errs = append(errs, err)
		}
	}

	for _, a := range activeIncidents {
		if err := i.GitlabGraphQLClient.CreateLinkedResource(
			issue.ID,
			a.HTMLURL,
			fmt.Sprintf("🚨 %s", a.Title),
		); err != nil {
			errs = append(errs, err)
		}
	}
	return woodhouse.MultiError(errs)
}

type OnCallGitLabUser struct {
	Mention              string
	Username             string
	UserID               int
	UsernameLookupFailed bool
	IsShadow             bool
}

type OnCallGitLabUsers struct {
	IMOCs             []*OnCallGitLabUser
	EOCs              []*OnCallGitLabUser
	IncidentAssignees []*OnCallGitLabUser
}

func (i *DeclareModalHandler) getGitLabUsersForOnCalls(ctx context.Context, severity Severity, payload *IncidentPayload) (*OnCallGitLabUsers, error) {
	var errs []error

	gitLabUsersIMOC, err := i.getGitLabUsersForEscalationPolicy(ctx, i.PagerdutyEscalationPolicyIMOC)
	if err != nil {
		errs = append(errs, err)
	}

	gitLabUsersEOC, err := i.getGitLabUsersForEscalationPolicy(ctx, i.PagerdutyEscalationPolicyEOC)
	if err != nil {
		errs = append(errs, err)
	}

	onCallGitLabUsers := OnCallGitLabUsers{
		IMOCs: gitLabUsersIMOC,
		EOCs:  gitLabUsersEOC,
	}

	// External Severity 1 and Severity 2 incidents are only assigned to the IMOC if paged (IM selected),
	// everything else, including all internally facing issues are assigned to the EOC.
	if !payload.IsInternallyFacing() && severity.IsSev1Sev2() && payload.IMSelected() {
		onCallGitLabUsers.IncidentAssignees = gitLabUsersIMOC
	} else {
		onCallGitLabUsers.IncidentAssignees = gitLabUsersEOC
	}

	return &onCallGitLabUsers, woodhouse.MultiError(errs)
}

func (i *DeclareModalHandler) createIssue(
	ctx context.Context,
	event *instrumentation.Event,
	payload *IncidentPayload,
	username string,
	onCallGitLabUsers *OnCallGitLabUsers,
	incidentLabels []string,
) (*gitlab.Issue, error) {
	title := payload.Title()
	severityLabel := payload.SeverityLabel()
	severity, err := ParseSeverityFromIssueLabel(severityLabel)
	if err != nil {
		return nil, err
	}

	confidential := payload.IsConfidential()

	var description bytes.Buffer
	incidentTemplate, err := i.getIncidentTemplate(ctx)
	if err != nil {
		return nil, err
	}

	incidentTemplate = slackutil.ProcessExecSummary(
		slackutil.ProcessIssueTemplate(incidentTemplate),
		severity.IsSev1Sev2(),
	)
	descTmpl := template.Must(template.New("description").Parse(incidentTemplate))

	err = descTmpl.Execute(&description, struct {
		Date, Time, Username, EOCMentions, IMOCMentions string
	}{
		Date:         time.Now().UTC().Format("2006-01-02"),
		Time:         time.Now().UTC().Format("15:04"),
		Username:     username,
		EOCMentions:  strings.Join(mentionsForUsers(onCallGitLabUsers.EOCs), ", "),
		IMOCMentions: strings.Join(mentionsForUsers(onCallGitLabUsers.IMOCs), ", "),
	})
	if err != nil {
		return nil, err
	}
	descStr := description.String()

	labels := gitlab.Labels{gitlabutil.Labels["IncidentDeclare"], severityLabel}
	labels = append(labels, payload.ExtraLabels()...)
	labels = append(labels, incidentLabels...)

	if severity.IsSev1Sev2() {
		labels = append(
			labels,
			gitlabutil.Labels["BlocksFeatureFlags"],
			gitlabutil.Labels["BlocksDeployments"],
		)
	}

	issue, _, err := i.GitlabClient.Issues.CreateIssue(i.GitlabProductionProjectPath, &gitlab.CreateIssueOptions{
		Title:        &title,
		Description:  &descStr,
		Labels:       labels,
		AssigneeIDs:  idsForUsers(onCallGitLabUsers.IncidentAssignees),
		IssueType:    &i.GitLabIssueType,
		Confidential: &confidential,
	}, gitlab.WithContext(ctx))
	if err != nil {
		return nil, err
	}

	issueSubmited, _, err := i.GitlabClient.Issues.GetIssue(i.GitlabProductionProjectPath, issue.IID)
	if err != nil {
		return nil, err
	}

	descUpdated := slackutil.ProcessRelatedIssues(
		issueSubmited.Description,
		issue.WebURL,
		issue.Title,
	)

	issue, _, err = i.GitlabClient.Issues.UpdateIssue(i.GitlabProductionProjectPath, issue.IID, &gitlab.UpdateIssueOptions{
		Description: &descUpdated,
	}, gitlab.WithContext(ctx))
	if err != nil {
		return nil, err
	}

	return issue, nil
}

//nolint:staticcheck // SA1019 https://github.com/slack-go/slack/issues/876
func (i *DeclareModalHandler) createChannel(ctx context.Context, issueTitle string) (*slack.Channel, error) {
	// We don't have an issue yet, so use the current time as a placeholder. If we
	// get an issue later, we can update the channel's name.
	issueSlug := time.Now().UTC().Unix()
	chName := i.incidentChannelName(issueSlug)
	incidentChannel, err := i.SlackClient.CreateConversationContext(ctx, chName, false)
	if err != nil {
		return nil, fmt.Errorf("error creating slack channel: %s", err)
	}
	channelPurpose := i.appendIncidentCall(
		fmt.Sprintf(
			`Incident "%s"`,
			woodhouse.TruncateString(issueTitle, issueTitleMaxLengthForSlack),
		),
	)
	_, err = i.SlackClient.SetTopicOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return incidentChannel, fmt.Errorf("error setting channel topic: %s", err)
	}

	return incidentChannel, nil
}

func (i *DeclareModalHandler) incidentChannelName(issueSlug int64) string {
	baseName := i.IncidentChannelNamePrefix
	if baseName == "" {
		projectPathParts := strings.Split(i.GitlabProductionProjectPath, "/")
		projectName := projectPathParts[len(projectPathParts)-1]
		baseName = projectName
	}
	return fmt.Sprintf("%s-%d", baseName, issueSlug)
}

//nolint:staticcheck // SA1019 https://github.com/slack-go/slack/issues/876
func (i *DeclareModalHandler) linkChannelToIssue(ctx context.Context, incidentChannel *slack.Channel, issue *gitlab.Issue) error {
	channelPurpose := i.appendIncidentCall(
		fmt.Sprintf(
			`Incident "<%s|%s>"`,
			issue.WebURL,
			woodhouse.TruncateString(issue.Title, issueTitleMaxLengthForSlack)),
	)
	_, err := i.SlackClient.SetPurposeOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return fmt.Errorf("error setting channel purpose: %s", err)
	}
	_, err = i.SlackClient.SetTopicOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return fmt.Errorf("error setting channel topic: %s", err)
	}
	return nil
}

func (i *DeclareModalHandler) performOptionalTasks(
	ctx context.Context, payload *IncidentPayload, incident IncidentContext, title string,
	appendToThread func(string) error,
) []error {
	pagerMsg := fmt.Sprintf("Incident declared: %s", title)
	slackURL := slackapps.ChannelLink(incident.AnnounceChannelID, incident.TeamID)
	var taskErrs []error
	for _, selectedTask := range payload.SelectedTasks() {
		switch selectedTask {
		case EOCSelection:
			_ = appendToThread(":calling: Engineer On-Call (EOC)")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyEOC, pagerMsg, slackURL); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging EOC: %s", err))
			}
		case IMSelection:
			_ = appendToThread(":calling: Incident Manager (IM)")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyIMOC, pagerMsg, slackURL); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging IM: %s", err))
			}
		case CMOCSelection:
			_ = appendToThread(":calling: Communications Manager On-Call (CMOC)")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyCMOC, pagerMsg, slackURL); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging CMOC: %s", err))
			}
		default:
			err := fmt.Errorf("unknown task: %s", selectedTask)
			taskErrs = append(taskErrs, err)
		}
	}
	return taskErrs
}

func (i *DeclareModalHandler) appendIncidentCall(msg string) string {
	return fmt.Sprintf("%s\n\n%s", msg, strings.Replace(i.IncidentCallURL, " ", "", -1))
}

func (i *DeclareModalHandler) getGitLabUsersForEscalationPolicy(ctx context.Context, escalationPolicy string) ([]*OnCallGitLabUser, error) {
	onCallUsers, err := i.Pager.GetOnCallUsersByEscalationPolicy(
		ctx,
		[]string{escalationPolicy},
		nil,
		nil,
	)
	if err != nil {
		return nil, err
	}
	return i.getGitLabUsers(ctx, onCallUsers)
}

func (i *DeclareModalHandler) getGitLabUsers(ctx context.Context, onCallUsers []*pagerduty.OnCallUser) ([]*OnCallGitLabUser, error) {
	var gitLabUsers []*OnCallGitLabUser
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		for _, u := range onCallUsers {
			user, err := u.MapToGitLabUserObject(ctx, i.GitlabClient)
			if err == pagerduty.ErrOnCallUserToGitLabUserEmpty {
				// The user was not resolved via their emails.
				// for users that do not have a public email we will
				// default to using their profile email
				emailParts := strings.Split(u.Email, "@")
				gitLabUsers = append(gitLabUsers, &OnCallGitLabUser{
					Mention:              "`" + emailParts[0] + " (user lookup failed)`",
					UsernameLookupFailed: true,
				})
				continue
			}
			if err != nil {
				// Wrap any other errors and return
				return retry.RetryableError(err)
			} else {
				gitLabUsers = append(gitLabUsers, &OnCallGitLabUser{
					Mention:              "@" + user.Username,
					Username:             user.Username,
					UserID:               user.ID,
					UsernameLookupFailed: false,
					IsShadow:             u.IsShadow,
				})
			}
		}
		return nil
	})

	// Order users so that shadows are last
	sort.Slice(gitLabUsers, func(i, _ int) bool { return !gitLabUsers[i].IsShadow })
	return gitLabUsers, err
}

func (i *DeclareModalHandler) getIncidentTemplate(ctx context.Context) (string, error) {
	var template string
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		templateBytes, _, err := i.GitlabClient.RepositoryFiles.GetRawFile(
			"gitlab-com/gl-infra/production", ".gitlab/issue_templates/incident.md",
			&gitlab.GetRawFileOptions{Ref: strptr("master")},
			gitlab.WithContext(ctx),
		)
		if err != nil {
			return retry.RetryableError(err)
		}
		template = string(templateBytes)
		return nil
	})
	if err != nil {
		return "", err
	}

	return template, nil
}

func strptr(s string) *string {
	return &s
}

func idsForUsers(users []*OnCallGitLabUser) []int {
	userIDs := []int{}
	for _, u := range users {
		if !u.UsernameLookupFailed {
			userIDs = append(userIDs, u.UserID)
		}
	}
	return userIDs
}

func mentionsForUsers(users []*OnCallGitLabUser) []string {
	userMentions := []string{}

	for _, u := range users {
		mention := u.Mention
		if u.IsShadow {
			mention = mention + " (Shadow)"
		}

		userMentions = append(userMentions, mention)
	}
	return userMentions
}
