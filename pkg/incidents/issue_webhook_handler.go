package incidents

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlabutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type IssueWebhookHandler struct {
	GitlabGraphQLClient    *gitlabutil.GraphQLClient
	UpdateIncidentSeverity bool
	SlackClient            *slack.Client
	ChannelID              string
}

func (h *IssueWebhookHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)

	payloadBytes, err := io.ReadAll(req.Body)
	if err != nil {
		event.With("error", err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	webhookEvent, err := gitlab.ParseWebhook(gitlab.WebhookEventType(req), payloadBytes)
	if err != nil {
		event.With("error", err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	issueEvent, ok := webhookEvent.(*gitlab.IssueEvent)
	if !ok {
		event.With("error", "event is not an IssueEvent")
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	postedUpdate := false

	if IssueHasNotableUpdate(issueEvent) {
		if h.UpdateIncidentSeverity {
			if err := h.updateIncidentSeverity(issueEvent); err != nil {
				event.With("error", err)
				w.WriteHeader(http.StatusUnprocessableEntity)
				return
			}
		}
		if err := h.postMessage(req.Context(), issueEvent); err != nil {
			event.With("error", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		postedUpdate = true
	}

	event.With("posted_update", postedUpdate)
}

func IssueHasNotableUpdate(issueEvent *gitlab.IssueEvent) bool {
	var severityLabels []string
	for _, i := range []int{1, 2, 3, 4} {
		severityLabels = append(severityLabels, Severity(i).IssueLabel())
	}

	return isNewNonWoodhouseIncident(issueEvent) ||
		isReopenedActiveIncident(issueEvent) ||
		isRelabeledSignificantly(issueEvent, append(severityLabels, ActiveIncidentLabel)...)
}

func isNewNonWoodhouseIncident(issueEvent *gitlab.IssueEvent) bool {
	return issueEvent.ObjectAttributes.Action == "open" &&
		labelsInclude(issueEvent.Labels, "incident") &&
		!labelsInclude(issueEvent.Labels, "Source::IMA::IncidentDeclare")
}

func isReopenedActiveIncident(issueEvent *gitlab.IssueEvent) bool {
	return issueEvent.ObjectAttributes.Action == "reopen" &&
		labelsInclude(issueEvent.Labels, "incident")
}

func isRelabeledSignificantly(issueEvent *gitlab.IssueEvent, labels ...string) bool {
	for _, label := range labels {
		if issueEvent.ObjectAttributes.Action == "update" &&
			labelsInclude(issueEvent.Labels, "incident") &&
			labelsInclude(issueEvent.Changes.Labels.Current, label) &&
			!labelsInclude(issueEvent.Changes.Labels.Previous, label) {
			return true
		}
	}
	return false
}

func (h *IssueWebhookHandler) postMessage(ctx context.Context, issue *gitlab.IssueEvent) error {
	var severityEmoji string
	for _, label := range issue.Labels {
		if strings.HasPrefix(label.Name, "severity::") {
			var err error
			sev, err := ParseSeverityFromIssueLabel(label.Name)
			if err != nil {
				return err
			}
			severityEmoji = sev.SlackEmoji()
		}
	}

	_, _, _, err := h.SlackClient.SendMessageContext(
		ctx, h.ChannelID,
		slack.MsgOptionText(
			fmt.Sprintf(
				"Incident %s '%s' %s: <%s>", severityEmoji, issue.ObjectAttributes.Title,
				pastParticiple(issue.ObjectAttributes.Action), issue.ObjectAttributes.URL,
			),
			false,
		),
	)
	return err
}

func (h *IssueWebhookHandler) updateIncidentSeverity(issue *gitlab.IssueEvent) error {
	for _, label := range issue.Labels {
		if strings.HasPrefix(label.Name, "severity::") {
			sev, err := ParseSeverityFromIssueLabel(label.Name)
			if err != nil {
				return err
			}
			if err := h.GitlabGraphQLClient.SetIssueSeverity(
				issue.ObjectAttributes.IID,
				issue.Project.PathWithNamespace, // h.GitlabIncidentIssueProjectPath,
				sev.IncidentSeverity(),
			); err != nil {
				return err
			}
			// It's only possible to set one scoped severity label
			// so we can stop after the first one
			break
		}
	}
	return nil
}

func labelsInclude(labels []gitlab.Label, label string) bool {
	for _, existingLabel := range labels {
		if existingLabel.Name == label {
			return true
		}
	}
	return false
}

func pastParticiple(verb string) string {
	if strings.HasSuffix(verb, "e") {
		return verb + "d"
	}
	return verb + "ed"
}
