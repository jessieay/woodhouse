package instrumentation

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	httpRequestsTotal          *prometheus.CounterVec
	httpRequestDurationSeconds *prometheus.HistogramVec
	httpRequestsErrorsTotal    *prometheus.CounterVec
)

func NewInstrumentedHTTPMiddleware(logger log.Logger, metricsRegistry prometheus.Registerer, unloggedPaths []string, next http.Handler) http.Handler {
	registerMetrics(metricsRegistry)
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		event := NewEvent()
		reqWithEvent := req.Clone(context.WithValue(req.Context(), EventContextKey{}, event))
		respStatusSpy := &responseWriterSpy{ResponseWriter: w}

		duration := MeasureTime(func() {
			next.ServeHTTP(respStatusSpy, reqWithEvent)
		})

		status := respStatusSpy.statusCode
		if status == 0 {
			status = http.StatusOK
		}
		event.With("event", "http_request", "status", status, "duration_s", duration, "path", req.URL.Path, "method", req.Method)
		if event.Get("route") == nil {
			event.With("route", "UNKNOWN")
		}

		httpRequestsTotal.WithLabelValues(toStr(event.Get("route")), toStr(event.Get("status"))).Inc()
		httpRequestDurationSeconds.WithLabelValues(toStr(event.Get("route")), toStr(event.Get("status"))).Observe(duration)
		if event.Get("error") != nil {
			httpRequestsErrorsTotal.WithLabelValues(toStr(event.Get("route")), toStr(event.Get("status"))).Inc()
		}

		if !include(unloggedPaths, req.URL.Path) {
			logger.Log(event.All()...)
		}
	})
}

type EventContextKey struct{}

type responseWriterSpy struct {
	statusCode int
	http.ResponseWriter
}

func (w *responseWriterSpy) WriteHeader(statusCode int) {
	w.statusCode = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}

func registerMetrics(metricsRegistry prometheus.Registerer) {
	factory := promauto.With(metricsRegistry)

	httpRequestsTotal = factory.NewCounterVec(prometheus.CounterOpts{
		Namespace: "woodhouse",
		Name:      "http_requests_total",
		Help:      "Number of HTTP requests handled.",
	}, []string{"route", "status"})

	httpRequestDurationSeconds = factory.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "woodhouse",
		Name:      "http_request_duration_seconds",
		Help:      "Durations of HTTP requests.",
		Buckets:   prometheus.DefBuckets,
	}, []string{"route", "status"})

	httpRequestsErrorsTotal = factory.NewCounterVec(prometheus.CounterOpts{
		Namespace: "woodhouse",
		Name:      "http_requests_errors_total",
		Help:      "Number of HTTP that resulted in an error, regardless of status.",
	}, []string{"route", "status"})
}

func include(l []string, s string) bool {
	for _, e := range l {
		if e == s {
			return true
		}
	}
	return false
}

func toStr(v interface{}) string {
	return fmt.Sprintf("%v", v)
}
