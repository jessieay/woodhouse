package gitlabutil

import (
	"bytes"
	"context"
	"os"
	"strings"
	"text/template"

	"github.com/go-kit/kit/log"
	"github.com/xanzy/go-gitlab"
)

func NotifyMergeRequestFromMirrorSource(ctx context.Context, client *gitlab.Client, logger log.Logger, mirrorProjectPath, pipelineStatus string, dryRun bool) error {
	projPath := mirrorProjectPath
	if projPath == "" {
		projPath = os.Getenv("CI_PROJECT_PATH")
	}
	logger = log.With(logger, "project_path", projPath)

	commit := os.Getenv("CI_COMMIT_SHA")
	commitMrs, _, err := client.Commits.GetMergeRequestsByCommit(projPath, commit, gitlab.WithContext(ctx))
	if err != nil {
		return err
	}
	var mrURLs []string
	for _, mr := range commitMrs {
		mrURLs = append(mrURLs, mr.WebURL)
	}
	logger.Log("msg", "found MRs for commit", "commit", commit, "merge_requests", strings.Join(mrURLs, ", "))

	noteTemplate, err := template.New("note").Parse(noteBodyTemplate)
	if err != nil {
		return err
	}
	var noteBodyBuf bytes.Buffer
	err = noteTemplate.Execute(&noteBodyBuf, struct {
		URL, Status string
	}{
		URL: os.Getenv("CI_PIPELINE_URL"), Status: pipelineStatus,
	})
	if err != nil {
		return err
	}
	noteBody := noteBodyBuf.String()

	for _, mr := range commitMrs {
		if dryRun {
			logger.Log("msg", "would link merge request to this pipeline", "merge_request", mr.WebURL)
		} else {
			logger.Log("msg", "linking merge request to this pipeline", "merge_request", mr.WebURL)
			_, _, err := client.Notes.CreateMergeRequestNote(projPath, mr.IID, &gitlab.CreateMergeRequestNoteOptions{Body: &noteBody}, gitlab.WithContext(ctx))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

const noteBodyTemplate = `A pipeline is running on a mirror related to this merge request.

Status: {{ .Status }}

{{ .URL }}
`
