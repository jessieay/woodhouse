package gitlabutil

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	httpretryable "github.com/hashicorp/go-retryablehttp"
)

type GraphQLClient struct {
	client     *httpretryable.Client
	token, url string
}

func NewGraphQLClient(client *httpretryable.Client, token, url string) *GraphQLClient {
	return &GraphQLClient{
		client: client,
		token:  token,
		url:    url,
	}
}

type SetSeverityResponse struct {
	Data struct {
		IssueSetSeverity struct{} `json:"issueSetSeverity"`
	} `json:"data"`
	Errors []struct {
		Message string `json:"message"`
	} `json:"errors"`
}

func (c *GraphQLClient) CreateLinkedResource(id int, link, linkText string) error {
	postResp, err := c.postGraphQL(issuableResourceLinkCreate(id, link, linkText, "general"))
	if err != nil {
		return fmt.Errorf("CreateLinkedResource postGraphQL: %w", err)
	}

	decoder := json.NewDecoder(bytes.NewReader(postResp))
	var setSeverityResponse SetSeverityResponse
	if err := decoder.Decode(&setSeverityResponse); err != nil {
		return fmt.Errorf("CreateLinkedResource JSON decode resp: %w", err)
	}

	if len(setSeverityResponse.Errors) > 0 {
		return fmt.Errorf("CreateLinkedResource error adding resource link, got: %v", setSeverityResponse.Errors)
	}

	return nil
}

func (c *GraphQLClient) SetIssueSeverity(iid int, projectPath, severity string) error {
	postResp, err := c.postGraphQL(issueSetSeverity(iid, projectPath, severity))
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(bytes.NewReader(postResp))
	var setSeverityResponse SetSeverityResponse
	if err := decoder.Decode(&setSeverityResponse); err != nil {
		return err
	}

	if len(setSeverityResponse.Errors) > 0 {
		return fmt.Errorf("Error setting severity, got: %v", setSeverityResponse.Errors)
	}

	return nil
}

func (c *GraphQLClient) postGraphQL(body string) ([]byte, error) {
	req, err := httpretryable.NewRequest(
		"POST", c.url,
		strings.NewReader(body),
	)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Unable to post to %s, received %d response", c.url, resp.StatusCode)
	}

	postResp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return postResp, nil
}

func issueSetSeverity(iid int, projectPath, severity string) string {
	// https://docs.gitlab.com/ee/api/graphql/reference/#mutationissuesetseverity
	return fmt.Sprintf(`{ "query": "mutation { issueSetSeverity(input: { iid: \"%d\", projectPath: \"%s\", severity: %s }) { errors } }" }`, iid, projectPath, severity)
}

func issuableResourceLinkCreate(id int, link, linkText, linkType string) string {
	// https://docs.gitlab.com/ee/api/graphql/reference/#mutationissuableresourcelinkcreate
	return fmt.Sprintf(`{ "query": "mutation { issuableResourceLinkCreate(input: { id: \"gid://gitlab/Issue/%d\", link: \"%s\", linkText: \"%s\", linkType: %s }) { errors } }" }`, id, link, linkText, linkType)
}
