package slackutil

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/sethvargo/go-retry"
	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
)

//nolint:staticcheck // SA1019 https://github.com/slack-go/slack/issues/876
func ArchiveIncidentChannels(
	ctx context.Context, logger log.Logger, client *slack.Client,
	gitlabClient *gitlab.Client,
	gitlabProductionProjectPath string,
	namePrefix string, dryRun bool,
) error {
	self, err := client.AuthTestContext(ctx)
	if err != nil {
		return fmt.Errorf("error getting own identity: %s", err)
	}

	var channels []slack.Channel
	var nextCursor string

	logger.Log("msg", "finding channels", "username", self.User, "user_id", self.UserID)
	listParams := &slack.GetConversationsParameters{ExcludeArchived: true}
	for {
		err = retry.Exponential(ctx, 10*time.Second, func(ctx context.Context) error {
			var err error
			channels, nextCursor, err = client.GetConversationsContext(ctx, listParams)
			if _, ok := err.(*slack.RateLimitedError); ok {
				logger.Log("msg", "slack rate limit, backing off")
				return retry.RetryableError(err)
			}
			return err
		})
		if err != nil {
			return err
		}

		for _, channel := range channels {
			if strings.HasPrefix(channel.Name, namePrefix+"-") {
				createdAt := channel.Created.Time().UTC()
				logger.Log("msg", "found channel", "channel", channel.Name, "creator", channel.Creator, "created_at", createdAt.String())

				suffix := channel.Name[len(namePrefix+"-"):]
				incidentID, err := strconv.Atoi(suffix)
				if err != nil {
					logger.Log("msg", "channel name did not have numeric incident id suffix", "channel", channel.Name)
					continue
				}

				var issue *gitlab.Issue
				var resp *gitlab.Response

				err = retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
					var err error
					issue, resp, err = gitlabClient.Issues.GetIssue(gitlabProductionProjectPath, incidentID, gitlab.WithContext(ctx))
					if err != nil && (resp.StatusCode == 418 || resp.StatusCode >= 500) {
						return retry.RetryableError(err)
					}
					return err
				})
				if err != nil {
					logger.Log("msg", "could not find issue for incident channel", "channel", channel.Name, "err", err)
					continue
				}

				incidentReviewNotificationText := "<!here> Thanks for taking part in this incident :fire:! It looks like this incident has the `review-requested` label, :clock3: it's time for an <https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#incident-review|Incident Review Schedule>! (_If you've scheduled a discussion already, please make sure you apply the `Incident::Review-Scheduled` label to the incident issue so I would stop reminding you!_ :gitlab-bot:)\n\nPlease head to the <https://docs.google.com/document/d/1jrX-Z2NJrNjBBcywY7emQKwaKRqVAlDRdGG0Krk76ys/edit#heading=h.qw1ojf4uj01q|Incident Review meeting Agenda> and add your incident notes to the coming session, the time of these sessions can be <https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#synchronous-review-meeting-sessions|found here>. Links to :zoom: can be found in the :google_calendar: <https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar|GitLab Team Meetings> calendar.\n\nIf you prefer to complete this review asynchronously, please state so in the comment section, fill the _Incident Review Section_ in the description body of the incident issue, and make sure to apply the `Incident::Review-Scheduled` label until the review is complete."

				reviewRequested := false
				reviewCompleted := false
				reviewScheduled := false

				for _, label := range issue.Labels {
					if label == "review-requested" {
						reviewRequested = true
					}
					if label == "Incident::Review-Completed" {
						reviewCompleted = true
					}
					if label == "Incident::Review-Scheduled" {
						reviewScheduled = true
					}
				}

				// Get the weekday of today
				weekday := time.Now().Weekday().String()

				// Remind qualifying slack channels to add incident to the Incident Review meeting
				if (weekday == "Friday" || weekday == "Monday") && reviewRequested && !reviewScheduled && !reviewCompleted {
					if dryRun {
						logger.Log("msg", "did not notify channel becuase this is a dry run", "channel", channel.Name)
					} else {
						msg := slack.MsgOptionText(incidentReviewNotificationText, false)
						logger.Log("msg", "notifying of incident review discussion", "channel", channel.Name)
						if channelID, timestamp, err := client.PostMessage(channel.ID, msg, slack.MsgOptionDisableLinkUnfurl()); err != nil {
							logger.Log("msg", timestamp, "failed to post message to channel", "channel", channelID, "type", "incident review notification", "err", err)
						}
					}
				}
				if channel.Creator == self.UserID && (!reviewRequested || reviewCompleted) && issue.State == "closed" {
					if dryRun {
						logger.Log("msg", "did not archive channel because this is a dry run", "channel", channel.Name)
					} else {
						logger.Log("msg", "archiving channel", "channel", channel.Name)
						if err := client.ArchiveConversationContext(ctx, channel.ID); err != nil {
							logger.Log("msg", "could not archive incident channel", "channel", channel.Name, "err", err)
							continue
						}
					}
				}
			}
		}
		if nextCursor == "" {
			return nil
		}
		listParams.Cursor = nextCursor
	}
}
