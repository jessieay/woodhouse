package slackutil

import (
	"context"
	"reflect"
	"sort"
	"strings"

	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"

	"github.com/go-kit/kit/log"
	"github.com/slack-go/slack"
)

func UpdateOncallUsergroups(
	ctx context.Context, logger log.Logger, client *slack.Client,
	pager pagerduty.Pager,
	groupID string, escalationPolicyID string, dryRun bool,
) error {
	logger.Log("msg", "finding on-call users in PagerDuty", "escalation_policy_id", escalationPolicyID)

	var slackUserIDs []string
	pdOnCallUsers, err := pager.GetOnCallUsersByEscalationPolicy(ctx, []string{escalationPolicyID}, nil, nil)
	if err != nil {
		return err
	}

	logger.Log("msg", "resolving slack users")
	for _, pdUser := range pdOnCallUsers {
		var emails []string
		emails = append(emails, pdUser.Email)
		emails = append(emails, pdUser.SecondaryEmails...)
		for _, email := range emails {
			slackUser, err := client.GetUserByEmailContext(ctx, email)
			if err != nil {
				if slackErr, ok := err.(slack.SlackErrorResponse); ok {
					if slackErr.Err == "users_not_found" {
						continue
					}
				}
				return err
			}
			slackUserIDs = append(slackUserIDs, slackUser.ID)
		}
	}

	logger.Log("msg", "looking up current slack group members", "group_id", groupID)
	groupMembers, err := client.GetUserGroupMembersContext(ctx, groupID)
	if err != nil {
		return err
	}

	slackUserIDs = deduplicateStrings(slackUserIDs)

	sort.Strings(slackUserIDs)
	sort.Strings(groupMembers)

	if reflect.DeepEqual(slackUserIDs, groupMembers) {
		logger.Log("msg", "on-call users matched group members, no update needed", "slack_user_ids", strings.Join(slackUserIDs, ","))
		return nil
	}

	if dryRun {
		logger.Log("msg", "would perform slack group update", "before", strings.Join(groupMembers, ","), "after", strings.Join(slackUserIDs, ","))
		return nil
	}

	logger.Log("msg", "performing slack group update", "before", strings.Join(groupMembers, ","), "after", strings.Join(slackUserIDs, ","))

	_, err = client.UpdateUserGroupMembersContext(ctx, groupID, strings.Join(slackUserIDs, ","))
	if err != nil {
		return err
	}

	return nil
}

// https://stackoverflow.com/questions/66643946/how-to-remove-duplicates-strings-or-int-from-slice-in-go
func deduplicateStrings(strSlice []string) []string {
	allKeys := make(map[string]bool)
	var list []string
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
