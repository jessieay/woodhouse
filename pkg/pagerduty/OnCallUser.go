package pagerduty

import (
	"context"
	"fmt"
	"github.com/xanzy/go-gitlab"
)

type OnCallUser struct {
	Email           string
	IsShadow        bool
	SecondaryEmails []string
}

var ErrOnCallUserToGitLabUserEmpty = fmt.Errorf("OnCallUser could not be resolved to GitLab user")

func (o *OnCallUser) AllEmails() (emails []string) {
	emails = append(emails, o.Email)
	emails = append(emails, o.SecondaryEmails...)
	return emails
}

/**
getGitLabUsernameByEmail has 3 different return conditions:
- The API request failed => username is empty, err is non-nil
- The API request was okay, but no user / more than one was found => username is empty, err is ErrOnCallUserToGitLabUserEmpty
- The API request was okay, and we found a user => username is non-empty, err is nil

We need to return this, as we need to make use of the fact if the API errored out downstream.
*/
func (o *OnCallUser) getGitLabUsernameByEmail(ctx context.Context, gitlabClient *gitlab.Client, email string) (string, error) {
	users, _, err := gitlabClient.Users.ListUsers(&gitlab.ListUsersOptions{Search: &email}, gitlab.WithContext(ctx))
	if err != nil {
		return "", err
	}
	if len(users) != 1 {
		return "", ErrOnCallUserToGitLabUserEmpty
	}
	return users[0].Username, nil
}

// MapToGitLabUsername returns in the same manner as getGitLabUsernameByEmail, but iterates over all email addresses.
func (o *OnCallUser) MapToGitLabUsername(ctx context.Context, gitlabClient *gitlab.Client) (string, error) {
	var username string
	var err error
	for _, email := range o.AllEmails() {
		username, err = o.getGitLabUsernameByEmail(ctx, gitlabClient, email)
		if err == ErrOnCallUserToGitLabUserEmpty {
			// Intercept this specific error and ignore it.
			continue
		}
		if err != nil {
			// The API has encountered an error. Forward that to the caller.
			return "", err
		}
		// The first user we can resolve by email without error is returned
		if username != "" {
			return username, nil
		}
	}
	// otherwise indicate there was no result
	return "", ErrOnCallUserToGitLabUserEmpty
}

// MapToGitLabUserObject like MapToGitLabUsername has 3 different return conditions:
// - The API request failed => user is nil, err is non-nil
// - The API request was okay, but no user / more than one was found => user is nil, err is ErrOnCallUserToGitLabUserEmpty
// - The API request was okay, and we found a user => user is non-empty, err is nil
//
// We need to return this, as we need to make use of the fact if the API errored out downstream.
func (o *OnCallUser) MapToGitLabUserObject(ctx context.Context, gitlabClient *gitlab.Client) (*gitlab.User, error) {
	username, err := o.MapToGitLabUsername(ctx, gitlabClient)
	if err != nil {
		return nil, err
	}
	var users []*gitlab.User
	users, _, err = gitlabClient.Users.ListUsers(&gitlab.ListUsersOptions{Username: &username}, gitlab.WithContext(ctx))
	if err != nil {
		// The API has encountered an error. Forward that to the caller.
		return nil, err
	}
	if len(users) != 1 {
		// We could not resolve a distinct user.
		return nil, ErrOnCallUserToGitLabUserEmpty
	}
	return users[0], err
}
