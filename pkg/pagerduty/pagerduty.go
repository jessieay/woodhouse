package pagerduty

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	gopagerduty "github.com/PagerDuty/go-pagerduty"
	"github.com/rs/xid"
	"github.com/sethvargo/go-retry"
)

const OpenIncidentCountLimit = 3

type Pager struct {
	HTTPClient      *http.Client
	PagerdutyClient *gopagerduty.Client
}

// TODO replace HTTP client code with go-pagerduty client code. Apparently that
// library does support the Events V2 API. Initially, I mistakenly thought it
// didn't.
func (p *Pager) Page(ctx context.Context, integrationKey, message string, slackURL string) error {
	if integrationKey == "" {
		return errors.New("Integration key not set for this service")
	}

	hostname, err := os.Hostname()
	if err != nil {
		return err
	}

	dedupKey := xid.New().String()

	body, err := json.Marshal(PagerdutyEvent{
		RoutingKey:  integrationKey,
		DedupKey:    dedupKey,
		EventAction: "trigger",
		Payload: PagerdutyEventPayload{
			Summary:   message,
			Source:    hostname,
			Severity:  "info",
			Component: "woodhouse",
			CustomDetails: PagerdutyCustomDetails{
				Type: "woodhouse",
			},
		},
		Links: []PagerdutyLink{
			{Text: "Slack channel", Href: slackURL},
		},
	})
	if err != nil {
		return err
	}

	return retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		resp, err := p.HTTPClient.Post("https://events.pagerduty.com/v2/enqueue", "application/json", bytes.NewReader(body))
		if err != nil {
			return retry.RetryableError(err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != 202 {
			return retry.RetryableError(fmt.Errorf("expected HTTP status 202, got %d", resp.StatusCode))
		}
		return nil
	})
}

func (p *Pager) GetOnCallUsersByEscalationPolicy(ctx context.Context, escalationPolicies []string, since, until *time.Time) ([]*OnCallUser, error) {
	var onCallUsers []*OnCallUser

	policies := removeEmpty(escalationPolicies)
	if len(policies) == 0 {
		return nil, nil
	}
	err := retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		sinceStr := ""
		if since != nil {
			sinceStr = since.Format(time.RFC3339)
		}
		untilStr := ""
		if until != nil {
			untilStr = until.Format(time.RFC3339)
		}

		onCalls, err := p.PagerdutyClient.ListOnCalls(gopagerduty.ListOnCallOptions{
			EscalationPolicyIDs: policies,
			Since:               sinceStr,
			Until:               untilStr,
			Earliest:            true,
			Includes:            []string{"users"},
		})
		if err != nil {
			return retry.RetryableError(err)
		}
		if onCalls.More {
			return fmt.Errorf("expected only 1 page of results for escalation policies: %s", strings.Join(policies, ", "))
		}

		for _, onCall := range onCalls.OnCalls {
			// Only take the first on-call
			if onCall.EscalationLevel != 1 {
				continue
			}
			// If the user does not have a primary email address, skip it
			if onCall.User.Email == "" {
				continue
			}
			s, _ := regexp.Compile("[sS]hadow")
			isShadow := s.MatchString(onCall.Schedule.APIObject.Summary)
			onCallUser := &OnCallUser{
				Email:           onCall.User.Email,
				IsShadow:        isShadow,
				SecondaryEmails: nil,
			}
			// If the user has further email addresses as contact methods, append them as well.
			for _, method := range onCall.User.ContactMethods {
				if method.Type == "email_contact_method_reference" {
					methodFilled, _ := p.PagerdutyClient.GetUserContactMethod(onCall.User.ID, method.ID)
					if methodFilled != nil {
						onCallUser.SecondaryEmails = append(onCallUser.SecondaryEmails, methodFilled.Address)
					}
				}
			}
			onCallUsers = append(onCallUsers, onCallUser)
		}
		return nil
	})
	return onCallUsers, err
}

func (p *Pager) GetOnCallShiftStartAndEndTime(ctx context.Context, escalationPolicies []string, since, until *time.Time) (string, string, error) {
	policies := removeEmpty(escalationPolicies)
	if len(policies) == 0 {
		return "", "", nil
	}
	sinceStr := ""
	if since != nil {
		sinceStr = since.Format(time.RFC3339)
	}
	untilStr := ""
	if until != nil {
		untilStr = until.Format(time.RFC3339)
	}

	onCalls, err := p.PagerdutyClient.ListOnCalls(gopagerduty.ListOnCallOptions{
		EscalationPolicyIDs: policies,
		Since:               sinceStr,
		Until:               untilStr,
		Earliest:            true,
	})
	if err != nil {
		return "", "", err
	}
	if onCalls.More {
		return "", "", fmt.Errorf("expected only 1 page of results for escalation policies: %s", strings.Join(policies, ", "))
	}
	if len(onCalls.OnCalls) == 0 {
		return "", "", fmt.Errorf("no on-calls found for escalation policies: %s", strings.Join(policies, ", "))
	}

	for _, onCall := range onCalls.OnCalls {
		if onCall.EscalationLevel != 1 {
			continue
		}
		return onCall.Start, onCall.End, nil
	}

	return "", "", fmt.Errorf("no first-level on-calls found for escalation policies: %s", strings.Join(policies, ", "))
}

// AlertNameForLogEntry will try to find a field named "alertname" in the incident
// log. Otherwise it returns an empty string
func (p *Pager) AlertNameForLogEntry(logEntry string) (string, error) {
	l, err := p.PagerdutyClient.GetLogEntry(logEntry, gopagerduty.GetLogEntryOptions{
		Includes: []string{"channels"},
	})
	if err != nil {
		return "", fmt.Errorf("PagerdutyClient.GetLogEntry: %w", err)
	}

	details := l.CommonLogEntryField.Channel.Raw["details"].(map[string]interface{})
	var alertName string
	if alertVal, ok := details["alertname"]; ok {
		alertName = fmt.Sprintf("%v", alertVal)
	} else {
		alertName = ""
	}

	return alertName, nil
}

// OpenIncidentsForEscalationPolicy returns open incidents where open is defined as any
// incident that was triggered in the last 24 hours and has not been resolved.
// The number of open incidents returned is limited to 3
func (p *Pager) OpenIncidentsForEscalationPolicy(escalationPolicy string) ([]OpenIncident, error) {
	activeIncidents := []OpenIncident{}

	serviceIDs, err := p.ServicesForEscalationPolicy(escalationPolicy)
	if err != nil {
		return nil, fmt.Errorf("ServicesForEscalationPolicy: %w", err)
	}

	since := time.Now().UTC().Add(-24 * time.Hour).Format(time.RFC3339)
	resp, err := p.PagerdutyClient.ListIncidents(gopagerduty.ListIncidentsOptions{
		Statuses:   []string{"acknowledged", "triggered"},
		ServiceIDs: serviceIDs,
		Since:      since,
	})
	if err != nil {
		return nil, fmt.Errorf("PagedutyClient.ListIncidents: %w", err)
	}

	for i, incident := range resp.Incidents {
		if i >= OpenIncidentCountLimit {
			return activeIncidents, nil
		}

		title := incident.Title

		alertName, err := p.AlertNameForLogEntry(incident.FirstTriggerLogEntry.ID)
		if err != nil {
			return nil, fmt.Errorf("AlertNameForLogEntry: %w", err)
		}

		if alertName != "" {
			title = fmt.Sprintf("%s | %s", title, alertName)
		}

		activeIncidents = append(activeIncidents, OpenIncident{
			Title:     title,
			HTMLURL:   incident.APIObject.HTMLURL,
			AlertName: alertName,
		})
	}

	return activeIncidents, nil
}

func (p *Pager) ServicesForEscalationPolicy(escalationPolicy string) ([]string, error) {
	serviceIDs := []string{}

	resp, err := p.PagerdutyClient.GetEscalationPolicy(escalationPolicy, &gopagerduty.GetEscalationPolicyOptions{
		Includes: []string{"services"},
	})
	if err != nil {
		return nil, fmt.Errorf("PagedutyClient.GetEscalationPolicy: %w", err)
	}

	for _, s := range resp.Services {
		serviceIDs = append(serviceIDs, s.ID)
	}
	return serviceIDs, nil
}

type OpenIncident struct {
	HTMLURL   string
	Title     string
	AlertName string
}

type PagerdutyEvent struct {
	RoutingKey  string                `json:"routing_key"`
	DedupKey    string                `json:"dedup_key"`
	EventAction string                `json:"event_action"`
	Payload     PagerdutyEventPayload `json:"payload"`
	Links       []PagerdutyLink       `json:"links"`
}

type PagerdutyEventPayload struct {
	Summary       string                 `json:"summary"`
	Source        string                 `json:"source"`
	Severity      string                 `json:"severity"`
	Component     string                 `json:"component"`
	CustomDetails PagerdutyCustomDetails `json:"custom_details"`
}

type PagerdutyCustomDetails struct {
	Type string `json:"type"`
}

type PagerdutyLink struct {
	Href string `json:"href"`
	Text string `json:"text"`
}

func removeEmpty(arr []string) []string {
	var removedEmpties []string
	for _, s := range arr {
		if s != "" {
			removedEmpties = append(removedEmpties, s)
		}
	}
	return removedEmpties
}
