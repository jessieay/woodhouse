package alertmanager

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type SilencesPayload []Silence

type Silence struct {
	ID        string        `json:"id"`
	StartsAt  time.Time     `json:"startsAt"`
	UpdatedAt time.Time     `json:"updatedAt"`
	EndsAt    time.Time     `json:"endsAt"`
	Comment   string        `json:"comment"`
	CreatedBy string        `json:"createdBy"`
	Status    SilenceStatus `json:"status"`
	Matchers  []Matcher     `json:"matchers"`
}

type SilenceStatus struct {
	State string `json:"state"`
}

type Matcher struct {
	IsEqual bool   `json:"isEqual"`
	IsRegex bool   `json:"isRegex"`
	Name    string `json:"name"`
	Value   string `json:"value"`
}

func NewClient(httpClient *http.Client, baseURL string) *Client {
	return &Client{
		httpClient: httpClient,
		baseURL:    baseURL,
	}
}

type Client struct {
	httpClient *http.Client
	baseURL    string
}

func (c *Client) GetSilences(ctx context.Context) (*SilencesPayload, error) {
	var payload SilencesPayload

	req, err := http.NewRequestWithContext(ctx, "GET", c.baseURL+"/api/v2/silences", nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("request to %v failed, expected status code %v, got %v", "/api/v2/silences", http.StatusOK, resp.StatusCode)
	}

	err = json.NewDecoder(resp.Body).Decode(&payload)
	if err != nil {
		return nil, err
	}

	return &payload, nil
}
