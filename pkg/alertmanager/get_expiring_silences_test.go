package alertmanager

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

// amtool --alertmanager.url='http://127.0.0.1:9093' silence -o json | jq
var rawAlertmanagerJSON = `
[
  {
    "id": "4640fc20-0d81-484b-bfa4-ad8ed7c03f6f",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-27T15:37:24.231Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5754",
    "createdBy": "Rehab",
    "endsAt": "2021-11-01T14:51:39.323Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "LoggingServiceFluentdLogOutputErrorSLOViolation"
      }
    ],
    "startsAt": "2021-10-27T15:37:24.231Z"
  },
  {
    "id": "784219b3-c3bf-4233-83d4-2c1911d4e745",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-29T13:29:35.553Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5667",
    "createdBy": "hphilipps",
    "endsAt": "2021-11-05T13:27:10.082Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "aggregation",
        "value": "component"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_class",
        "value": "traffic_cessation"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_type",
        "value": "cause"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "component",
        "value": "nginx_ingress"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "type",
        "value": "nginx"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "NginxServiceNginxIngressTrafficCessation"
      }
    ],
    "startsAt": "2021-10-29T13:29:35.553Z"
  },
  {
    "id": "792c73a1-500e-4f49-b9b7-298590dcec0e",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-29T13:31:06.052Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5667",
    "createdBy": "hphilipps",
    "endsAt": "2021-11-05T13:30:50.968Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_class",
        "value": "traffic_cessation"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "component",
        "value": "nginx_ingress"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "type",
        "value": "api"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "ApiServiceNginxIngressTrafficCessation"
      }
    ],
    "startsAt": "2021-10-29T13:31:06.052Z"
  },
  {
    "id": "adb5dfcb-0915-413a-ac16-1c3a2d4d70af",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-29T20:20:04.849Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5833",
    "createdBy": "hmeyer",
    "endsAt": "2021-11-05T20:19:41.921Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_type",
        "value": "symptom"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "environment",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "instance",
        "value": "https://release.gitlab.net"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "job",
        "value": "blackbox"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "monitor",
        "value": "default"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "pager",
        "value": "pagerduty"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "provider",
        "value": "gcp"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "region",
        "value": "us-east"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "severity",
        "value": "s1"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "shard",
        "value": "default"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "stage",
        "value": "main"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "tier",
        "value": "sv"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "type",
        "value": "blackbox"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "BlackboxProbeFailuresVeryLong"
      }
    ],
    "startsAt": "2021-10-29T20:20:04.849Z"
  },
  {
    "id": "aba922c6-1198-454f-b1e4-c54996266ee8",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-26T23:46:03.477Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5752#note_707971131",
    "createdBy": "devin",
    "endsAt": "2021-11-09T23:45:53.385Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "aggregation",
        "value": "component"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_class",
        "value": "slo_violation"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_type",
        "value": "symptom"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "component",
        "value": "cluster_scaleups"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "environment",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "feature_category",
        "value": "not_owned"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "monitor",
        "value": "global"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "pager",
        "value": "pagerduty"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "rules_domain",
        "value": "general"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "severity",
        "value": "s2"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "sli_type",
        "value": "error"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "slo_alert",
        "value": "yes"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "stage",
        "value": "main"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "tier",
        "value": "inf"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "type",
        "value": "kube"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "user_impacting",
        "value": "no"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "KubeServiceClusterScaleupsErrorSLOViolation"
      }
    ],
    "startsAt": "2021-10-26T23:46:03.477Z"
  },
  {
    "id": "4a268462-7808-4166-a7db-e0df11827a21",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-26T23:46:42.925Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5773",
    "createdBy": "devin",
    "endsAt": "2021-11-09T23:46:20.414Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "aggregation",
        "value": "component"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_class",
        "value": "slo_violation"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_type",
        "value": "symptom"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "component",
        "value": "cluster_scaleups"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "environment",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "feature_category",
        "value": "not_owned"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "monitor",
        "value": "global"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "pager",
        "value": "pagerduty"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "rules_domain",
        "value": "general"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "severity",
        "value": "s2"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "sli_type",
        "value": "error"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "slo_alert",
        "value": "yes"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "stage",
        "value": "main"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "tier",
        "value": "inf"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "type",
        "value": "kube"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "user_impacting",
        "value": "no"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "KubeServiceClusterScaleupsErrorSLOViolation"
      }
    ],
    "startsAt": "2021-10-26T23:46:42.925Z"
  },
  {
    "id": "568f3424-ca15-446d-ab25-1c5c7dd72f96",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-26T23:47:17.857Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5752#note_707971131",
    "createdBy": "devin",
    "endsAt": "2021-11-09T23:47:07.021Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "aggregation",
        "value": "component"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_class",
        "value": "slo_violation"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_type",
        "value": "symptom"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "component",
        "value": "cluster_scaleups"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "environment",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "feature_category",
        "value": "not_owned"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "monitor",
        "value": "global"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "pager",
        "value": "pagerduty"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "rules_domain",
        "value": "general"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "severity",
        "value": "s2"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "sli_type",
        "value": "error"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "slo_alert",
        "value": "yes"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "stage",
        "value": "main"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "tier",
        "value": "inf"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "type",
        "value": "kube"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "user_impacting",
        "value": "no"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "KubeServiceClusterScaleupsErrorSLOViolation"
      }
    ],
    "startsAt": "2021-10-26T23:47:17.857Z"
  },
  {
    "id": "e8b55b05-b1c4-4b4b-89ff-525d1ff2a89d",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-27T13:48:38.532Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5812",
    "createdBy": "iwiedler",
    "endsAt": "2021-11-10T12:19:53.931Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "KubeContainersWaitingInError"
      }
    ],
    "startsAt": "2021-10-27T13:48:38.532Z"
  },
  {
    "id": "c93df046-e377-4dce-97a5-b0be97258027",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-27T16:29:48.084Z",
    "comment": "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5817",
    "createdBy": "hmeyer",
    "endsAt": "2021-11-10T16:29:35.970Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alert_type",
        "value": "cause"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "cluster",
        "value": "gprd-gitlab-gke"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "component",
        "value": "kube_persistent_volume_claim_disk_space"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "env",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "environment",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "monitor",
        "value": "default"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "pager",
        "value": "pagerduty"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "prometheus",
        "value": "monitoring/gitlab-monitoring-promethe-prometheus"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "provider",
        "value": "gcp"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "rules_domain",
        "value": "general"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "stage",
        "value": "main"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "tier",
        "value": "inf"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "type",
        "value": "kube"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "alertname",
        "value": "component_saturation_slo_out_of_bounds"
      }
    ],
    "startsAt": "2021-10-27T16:29:48.084Z"
  },
  {
    "id": "1e01ce5f-1bb5-4b0a-9609-1cc1984e03ca",
    "status": {
      "state": "active"
    },
    "updatedAt": "2021-10-20T12:51:30.883Z",
    "comment": "Part of https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/13886, implemented with https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5670. We're going to migrate monitoring of CI Runners service to a dedicated Prometheus servers, but first we need to confirm that all works properly and then we need to gracefully rollout the migration. We'll use the stage=\"cny\" to determine which part of the metrics should not be considered for alerts.",
    "createdBy": "Tomasz Maczukin (tomasz@gitla.com)",
    "endsAt": "2021-12-19T00:00:00.000Z",
    "matchers": [
      {
        "isEqual": true,
        "isRegex": false,
        "name": "tier",
        "value": "runners"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "environment",
        "value": "gprd"
      },
      {
        "isEqual": true,
        "isRegex": false,
        "name": "stage",
        "value": "cny"
      }
    ],
    "startsAt": "2021-10-20T12:51:30.883Z"
  }
]
`

func TestGetExpiringSilences(t *testing.T) {
	for _, tc := range []struct {
		name               string
		alertmanagerStatus int
		alertmanagerBody   string
		cutoff             time.Duration
		startTime          time.Time
		expectedSilenceIDs []string
		expectedErr        string
	}{
		{
			name:               "returns no silences on empty array",
			alertmanagerStatus: http.StatusOK,
			alertmanagerBody:   "[]",
			cutoff:             time.Minute,
			startTime:          time.Date(2021, 11, 1, 0, 0, 0, 0, time.UTC),
			expectedSilenceIDs: nil,
		},
		{
			name:               "returns error on empty body",
			alertmanagerStatus: http.StatusOK,
			alertmanagerBody:   "",
			cutoff:             time.Minute,
			startTime:          time.Date(2021, 11, 1, 0, 0, 0, 0, time.UTC),
			expectedSilenceIDs: nil,
			expectedErr:        "EOF",
		},
		{
			name:               "returns error on invalid JSON",
			alertmanagerStatus: http.StatusOK,
			alertmanagerBody:   "hello",
			cutoff:             time.Minute,
			startTime:          time.Date(2021, 11, 1, 0, 0, 0, 0, time.UTC),
			expectedSilenceIDs: nil,
			expectedErr:        "invalid character 'h' looking for beginning of value",
		},
		{
			name:               "returns error on invalid status code",
			alertmanagerStatus: http.StatusNotFound,
			alertmanagerBody:   "error 404",
			cutoff:             time.Minute,
			startTime:          time.Date(2021, 11, 1, 0, 0, 0, 0, time.UTC),
			expectedSilenceIDs: nil,
			expectedErr:        "request to /api/v2/silences failed, expected status code 200, got 404",
		},
		{
			name:               "returns no silences expiring in the next minute",
			alertmanagerStatus: http.StatusOK,
			alertmanagerBody:   rawAlertmanagerJSON,
			cutoff:             time.Minute,
			startTime:          time.Date(2021, 11, 1, 0, 0, 0, 0, time.UTC),
			expectedSilenceIDs: nil,
		},
		{
			name:               "returns one silence expiring in the next 24 hours",
			alertmanagerStatus: http.StatusOK,
			alertmanagerBody:   rawAlertmanagerJSON,
			cutoff:             time.Hour * 24,
			startTime:          time.Date(2021, 11, 1, 0, 0, 0, 0, time.UTC),
			expectedSilenceIDs: []string{"4640fc20-0d81-484b-bfa4-ad8ed7c03f6f"},
		},
		{
			name:               "returns plenty of silences expiring in the next 7 days",
			alertmanagerStatus: http.StatusOK,
			alertmanagerBody:   rawAlertmanagerJSON,
			cutoff:             time.Hour * 24 * 7,
			startTime:          time.Date(2021, 11, 1, 0, 0, 0, 0, time.UTC),
			expectedSilenceIDs: []string{
				"4640fc20-0d81-484b-bfa4-ad8ed7c03f6f",
				"784219b3-c3bf-4233-83d4-2c1911d4e745",
				"792c73a1-500e-4f49-b9b7-298590dcec0e",
				"adb5dfcb-0915-413a-ac16-1c3a2d4d70af",
			},
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()

			handler := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
				w.WriteHeader(tc.alertmanagerStatus)
				_, err := w.Write([]byte(tc.alertmanagerBody))
				require.Nil(t, err)
			})

			server := httptest.NewServer(handler)
			defer server.Close()

			silences, err := GetExpiringSilences(ctx, GetExpiringSilencesOptions{
				AlertmanagerClient: NewClient(server.Client(), server.URL),
				Cutoff:             tc.cutoff,
				StartTime:          tc.startTime,
			})
			if tc.expectedErr != "" {
				require.EqualError(t, err, tc.expectedErr)
			} else {
				require.Nil(t, err)
				var silenceIDs []string
				for _, silence := range silences {
					silenceIDs = append(silenceIDs, silence.ID)
				}
				assert.Equal(t, tc.expectedSilenceIDs, silenceIDs)
			}
		})
	}
}
