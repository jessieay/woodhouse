package alertmanager

import (
	"context"
	"sort"
	"time"
)

type GetExpiringSilencesOptions struct {
	AlertmanagerClient *Client
	Cutoff             time.Duration
	StartTime          time.Time
}

func GetExpiringSilences(ctx context.Context, options GetExpiringSilencesOptions) ([]Silence, error) {
	payload, err := options.AlertmanagerClient.GetSilences(ctx)
	if err != nil {
		return nil, err
	}

	expiresBefore := options.StartTime.Add(options.Cutoff)

	var silences []Silence

	for _, silence := range *payload {
		if silence.EndsAt.Before(options.StartTime) || silence.EndsAt.After(expiresBefore) {
			continue
		}
		silences = append(silences, silence)
	}

	sort.Slice(silences, func(i, j int) bool {
		return silences[i].EndsAt.Unix() < silences[j].EndsAt.Unix()
	})

	return silences, nil
}
