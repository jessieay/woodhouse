package statusio

import (
	"strconv"

	gostatusio "github.com/statusio/statusio-go"
)

type IncidentStatusList []IncidentStatus

type IncidentStatus struct {
	Name    string
	Status  gostatusio.Status
	Default bool
}

type Client struct {
	Api         StatusioHandler
	PageID      string
	PrivateMode bool
	baseURL     string
}

type Component struct {
	ID   string
	Name string
}

func (isl IncidentStatusList) GetDefaultStatusIndex() int {
	for i, status := range isl {
		if status.Default {
			return i
		}
	}

	return 0
}

func (is IncidentStatus) ToString() string {
	return strconv.Itoa(int(is.Status))
}
