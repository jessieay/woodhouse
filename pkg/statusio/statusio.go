//go:generate mockgen -package mocks -destination mocks/statusio.go . StatusioHandler

package statusio

import (
	"errors"
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"

	gostatusio "github.com/statusio/statusio-go"
)

const (
	NotifyEmail   = "1"
	NotifySMS     = "1"
	NotifyWebhook = "1"
	NotifyMSTeams = "1"
	NotifyIRC     = "1"
	NotifyHipchat = "1"
	NotifySlack   = "1"
	TweetUpdate   = "1"
)

var (
	IncidentStatuses = IncidentStatusList{
		IncidentStatus{Name: "Degraded Performance", Status: gostatusio.StatusDegradedPerformance, Default: true},
		IncidentStatus{Name: "Partial Service Disruption", Status: gostatusio.StatusPartialServiceDisruption},
		IncidentStatus{Name: "Security Event", Status: gostatusio.StatusSecurityEvent},
		IncidentStatus{Name: "Service Disruption", Status: gostatusio.StatusServiceDisruption},
	}

	componentsMatchExact = []string{
		"API",
		"Background Processing",
		"Canary",
		"Container Registry",
		"Git Operations",
		"GitLab Pages",
		"SAML SSO - GitLab SaaS",
	}

	componentsMatchRegex = []string{
		"^CI\\/CD.*",
	}

	// Default incident state "Investigating"
	incidentState = gostatusio.StateInvestigating
)

type StatusioHandler interface {
	IncidentCreate(incident gostatusio.Incident) (r gostatusio.IncidentCreateResponse, err error)
	ComponentList(statusPageID string) (r gostatusio.ComponentListResponse, err error)
}

func NewClient(api StatusioHandler, pageID string, baseURL string) *Client {
	client := Client{
		Api:     api,
		PageID:  pageID,
		baseURL: baseURL,
	}

	if baseURL == "" {
		client.baseURL = fmt.Sprintf("https://app.status.io/internal_view/%s", pageID)
	}

	if strings.Contains(client.baseURL, "/internal_view/") {
		client.PrivateMode = true
	}

	return &client
}

func (c *Client) CreateIncident(incident gostatusio.Incident, components []string, incidentStatusName string) (gostatusio.IncidentCreateResponse, error) {
	infrastructureAffected, err := c.getComponentsAndContainers(components)
	if err != nil {
		return gostatusio.IncidentCreateResponse{}, fmt.Errorf("unable to build components->containers listing: %w", err)
	}

	incidentStatus, err := c.getIncidentStatusByString(incidentStatusName)
	if err != nil {
		return gostatusio.IncidentCreateResponse{}, fmt.Errorf("unable to get incident status: %w", err)
	}

	incident.InfrastructureAffected = infrastructureAffected
	incident.StatuspageID = c.PageID
	incident.CurrentState = incidentState
	incident.CurrentStatus = incidentStatus

	incident.NotifyEmail = NotifyEmail
	incident.NotifySms = NotifySMS
	incident.NotifyWebhook = NotifyWebhook
	incident.Social = TweetUpdate
	incident.Msteams = NotifyMSTeams
	incident.Irc = NotifyIRC
	incident.Hipchat = NotifyHipchat
	incident.Slack = NotifySlack

	return c.Api.IncidentCreate(incident)
}

func (c *Client) BaseURL() string {
	return c.baseURL
}

func (c *Client) GetInfrastructureListFilterByFunction(description string, matcher func(name string) bool) ([]Component, error) {
	var filteredComponents []Component

	components, err := c.getInfrastructureList()
	if err != nil {
		return components, err
	}

	for _, component := range components {
		if matcher(component.Name) {
			filteredComponents = append(filteredComponents, component)
		}
	}

	if len(filteredComponents) == 0 {
		return filteredComponents, fmt.Errorf("no components matched '%s'", description)
	}

	return filteredComponents, err
}

func (c *Client) getInfrastructureList() ([]Component, error) {
	var components []Component

	fullComponentsList, err := c.Api.ComponentList(c.PageID)
	if err != nil {
		return components, err
	}

	var componentsMatchRegexCompiled []*regexp.Regexp
	for _, regex := range componentsMatchRegex {
		if regexCompiled, err := regexp.Compile(regex); err == nil {
			componentsMatchRegexCompiled = append(componentsMatchRegexCompiled, regexCompiled)
		}
	}

	for _, component := range fullComponentsList.Result {
		match := false
		for _, name := range componentsMatchExact {
			if component.Name == name {
				match = true
				components = append(components, Component{ID: component.ID, Name: component.Name})
				break
			}
		}

		if !match {
			for _, regex := range componentsMatchRegexCompiled {
				if regex.MatchString(component.Name) {
					components = append(components, Component{ID: component.ID, Name: component.Name})
					break
				}
			}
		}
	}

	if len(components) > 10 {
		// Slack will throw a "invalid_arguments" error if you try to show more than 10 components
		return []Component{}, errors.New("more than 10 components were matched")
	}

	sort.Slice(components, func(i, j int) bool {
		return components[i].Name < components[j].Name
	})

	return components, nil
}

func (c *Client) getComponentsAndContainers(components []string) ([]string, error) {
	var componentsAndContainers []string

	fullComponentsList, err := c.Api.ComponentList(c.PageID)
	if err != nil {
		return componentsAndContainers, err
	}

	for _, needle := range components {
		for _, component := range fullComponentsList.Result {
			if component.ID == needle {
				for _, container := range component.Containers {
					componentsAndContainers = append(componentsAndContainers, fmt.Sprintf("%s-%s", component.ID, container.ID))
				}
			}
		}
	}

	return componentsAndContainers, nil
}

func (c *Client) getIncidentStatusByString(code string) (gostatusio.Status, error) {
	status, err := strconv.Atoi(code)
	if err != nil {
		return gostatusio.Status(status), err
	}

	return gostatusio.Status(status), nil
}

func (c *Client) GetIncidentsBaseURL() string {
	if c.PrivateMode {
		return "https://app.status.io/pages/incident/" + c.PageID
	}

	return c.baseURL + "/pages/incident/" + c.PageID
}

func (c *Client) GetIncidentDashboardURL(incidentID string) string {
	return fmt.Sprintf("https://app.status.io/dashboard/%s/incident/%s/edit", c.PageID, incidentID)
}
