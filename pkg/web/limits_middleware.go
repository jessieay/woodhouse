package web

import (
	"context"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"golang.org/x/time/rate"
)

const maxRateBurst = 10

type LimitsMiddleware struct {
	maxReqBodySizeBytes int64
	maxReqDuration      time.Duration
	maxReqsPerSecond    int
	ipLimiters          map[string]*rate.Limiter
	rateLimiterLock     *sync.RWMutex
	next                http.Handler
}

func NewLimitsMiddleware(maxReqDuration time.Duration, maxReqBodySizeBytes int64, maxReqsPerSecond int, next http.Handler) http.Handler {
	return &LimitsMiddleware{
		maxReqBodySizeBytes: maxReqBodySizeBytes,
		maxReqDuration:      maxReqDuration,
		maxReqsPerSecond:    maxReqsPerSecond,
		ipLimiters:          map[string]*rate.Limiter{},
		rateLimiterLock:     new(sync.RWMutex),
		next:                next,
	}
}

func (m *LimitsMiddleware) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ip, err := requestIP(req)
	if err != nil {
		event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		event.With("error", err, "message", "error parsing request IP")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	limiter := m.getLimiter(ip)
	if !limiter.Allow() {
		w.WriteHeader(http.StatusTooManyRequests)
		return
	}

	req.Body = http.MaxBytesReader(w, req.Body, m.maxReqBodySizeBytes)
	ctx, cancel := context.WithTimeout(req.Context(), m.maxReqDuration)
	defer cancel()
	m.next.ServeHTTP(w, req.Clone(ctx))
}

func (m *LimitsMiddleware) getLimiter(ip string) *rate.Limiter {
	m.rateLimiterLock.RLock()
	limiter, ok := m.ipLimiters[ip]
	m.rateLimiterLock.RUnlock()
	if ok {
		return limiter
	}

	m.rateLimiterLock.Lock()
	defer m.rateLimiterLock.Unlock()

	// Check again, in case another thread got the lock first and created the
	// limiter.
	limiter, ok = m.ipLimiters[ip]
	if ok {
		return limiter
	}

	limiter = rate.NewLimiter(rate.Every(time.Second/time.Duration(m.maxReqsPerSecond)), maxRateBurst)
	m.ipLimiters[ip] = limiter
	return limiter
}

func requestIP(req *http.Request) (string, error) {
	proxiedIPsStr := req.Header.Get("X-Forwarded-For")
	if proxiedIPsStr != "" {
		proxiedIPs := strings.Split(proxiedIPsStr, ", ")
		return proxiedIPs[0], nil
	}

	host, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		return "", err
	}
	return host, nil
}
