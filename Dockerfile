# Build stage
FROM golang:1.18 as build

WORKDIR /app
COPY . /app
RUN make

# Final image
FROM alpine:3.16

COPY --from=build /app/woodhouse /bin/woodhouse
RUN apk add --no-cache git
CMD ["/bin/woodhouse"]
